#ifndef _TEEX_SSL_H_
#define _TEEX_SSL_H_


#ifndef _cplusplus
#include <stdbool.h>
#endif

#include <sgx_eid.h>
#include <sgx_report.h>
#include <sgx_dh.h>

#define TEEX_SSL_FLAGS_INVOLVE_IAS 0x00000001
#define TEEX_SSL_FLAGS_VERIFY_TPM 0x00000002


#ifdef __cplusplus
extern "C" {
#endif

typedef struct _TEEX_SSL TEEX_SSL;

typedef enum {
	TEEX_SSL_AUTO_DETECT = 0x1000,    // can only be used by TEEX_accept
	TEEX_SSL_MUTUAL_RA_INITIATOR = 0x1001,
	TEEX_SSL_MUTUAL_RA_RESPONDER = 0x1002,
	TEEX_SSL_SINGLE_RA_INITIATOR = 0x1003,
	TEEX_SSL_SINGLE_RA_RESPONDER = 0x1004,
	TEEX_SSL_NO_RA_INITIATOR = 0x1005,
	TEEX_SSL_NO_RA_RESPONDER = 0x1006
} TEEX_SSL_TYPE;

typedef bool (*TEEX_SSL_remote_attestation_callback)(const sgx_report_body_t *remote_enclave_report_body, const char *remote_enclave_quote_status, void *arg);
typedef bool (*TEEX_SSL_tpm_attestation_callback)(const sgx_dh_session_enclave_identity_t *daemon_enclave_identity, const void *tpm_quote, int tpm_quote_len, const void *other_info, int other_info_len, void *arg);    // XXX other_info may be the tpm measurement log, or a NULL-terminated (char const* const*) array of all pcr-extended sha1 hashs. it is always NULL temporarily.

TEEX_SSL *TEEX_SSL_new(TEEX_SSL_TYPE type);
void TEEX_SSL_free(TEEX_SSL *context);

// basic settings
void TEEX_SSL_set_self_eid(TEEX_SSL *context, sgx_enclave_id_t eid);
void TEEX_SSL_set_attributes(TEEX_SSL *context, int attr);
void TEEX_SSL_set_fd(TEEX_SSL *context, int fd);
int TEEX_SSL_get_fd(TEEX_SSL *context);

// settings about involving ias
void TEEX_SSL_set_attestation_callback(TEEX_SSL *context, TEEX_SSL_remote_attestation_callback callback, void *arg);
void TEEX_SSL_set_ias_client_cert_key(TEEX_SSL *context, const char *ias_client_cert_file, const char *ias_client_key_file);

// settings about verifing tpm
void TEEX_SSL_set_tpm_attestation_daemon_host_port(TEEX_SSL *context, const char *host, int port);
//void TEEX_SSL_set_tpm_ak_cert(TEEX_SSL *context, const char *tpm_ak_cert_file);
void TEEX_SSL_set_tpm_ak_pubkey(TEEX_SSL *context, const char *tpm_ak_pem_pubkey_file);  // XXX here, we use ak pem public key instead of cert temporarily
void TEEX_SSL_set_tpm_attestation_callback(TEEX_SSL *context, TEEX_SSL_tpm_attestation_callback callback, void *arg);

int TEEX_connect(TEEX_SSL *context);
int TEEX_accept(TEEX_SSL *context);

int TEEX_read(TEEX_SSL *context, void *buf, size_t count);
int TEEX_write(TEEX_SSL *context, const void *buf, size_t count);

int TEEX_readn(TEEX_SSL *context, void *buf, size_t n);
int TEEX_writen(TEEX_SSL *context, const void *buf, size_t n);


#ifdef __cplusplus
}
#endif


#endif

