#ifndef UTIL_H
#define UTIL_H


char* teex_new_uuid();

char* bytes32_from_uuid(char* uuid);

char* uuid_from_bytes32(char* bytes32);
#endif
