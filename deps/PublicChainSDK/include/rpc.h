#ifndef RPC_H
#define RPC_H

#include <iostream> 
#include <stdint.h>
#include <cjson/cJSON.h>

#include "ethrpc_type.h"
#include <curl/curl.h>

using namespace std;




class RPC{

public:

    RPC(const char* url);
    ~RPC();

    ResultCode call_rpc(int id, 
                        const char* method, 
                        cJSON *param,
                        string *result);



private:

    
    const char* url;  //default

    CURL* curl;
    struct curl_slist* headers;


};

#endif