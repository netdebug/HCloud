#ifndef __TEEX_ERROR_H__
#define __TEEX_ERROR_H__

#define TEEX_MK_ERROR(x)		(0x00000000|(x))

typedef enum _teex_status_t
{
	TEEX_SUCCESS						= TEEX_MK_ERROR(0x0000),
	TEEX_ERROR_OUT_MEMORY				= TEEX_MK_ERROR(0x0002),
	TEEX_ERROR_NETWORK					= TEEX_MK_ERROR(0x0003),
	TEEX_ERROR_INTERNAL					= TEEX_MK_ERROR(0x0004),	
	TEEX_ERROR_NULL_ARG					= TEEX_MK_ERROR(0x0005),

	TEEX_ERROR_INVALID_CHAINURL			= TEEX_MK_ERROR(0x1001),
	TEEX_ERROR_INVALID_PRIVATE_KEY		= TEEX_MK_ERROR(0x1002),
	TEEX_ERROR_INVALID_PUBLIC_KEY		= TEEX_MK_ERROR(0x1003),
	TEEX_ERROR_INVALID_CONTRACT1		= TEEX_MK_ERROR(0x1004),
	TEEX_ERROR_INVALID_CONTRACT2		= TEEX_MK_ERROR(0x1005),
	TEEX_ERROR_INVALID_TASKINFO			= TEEX_MK_ERROR(0x1006),
	TEEX_ERROR_INVALID_TASKID_BUF		= TEEX_MK_ERROR(0x1007),
	TEEX_ERROR_CHAIN_DISCONNECTED		= TEEX_MK_ERROR(0x1008),
	TEEX_ERROR_CREATE_TASK				= TEEX_MK_ERROR(0x1009),
	TEEX_ERROR_CONFIRM					= TEEX_MK_ERROR(0x100a),
	TEEX_ERROR_TIMEOUT					= TEEX_MK_ERROR(0x100b),

	TEEX_ERROR_INVALID_DSPT_ADDR		= TEEX_MK_ERROR(0x2001),
	TEEX_ERROR_INVALID_RESULT_BUF		= TEEX_MK_ERROR(0x2002),
	TEEX_ERROR_INVALID_SIZE_BUF			= TEEX_MK_ERROR(0x2003),
	TEEX_ERROR_INVALID_WORKER_BUF		= TEEX_MK_ERROR(0x2004),
	TEEX_ERROR_CONNECT_DISPATCHER		= TEEX_MK_ERROR(0x2005),
	TEEX_ERROR_INVALID_CLIENT_CERT		= TEEX_MK_ERROR(0x2006),
	TEEX_ERROR_ATTESTATION_FAILED		= TEEX_MK_ERROR(0x2007),
	TEEX_ERROR_INVALID_JSON				= TEEX_MK_ERROR(0x2008),
	TEEX_ERROR_INVALID_RESP_FORMAT		= TEEX_MK_ERROR(0x2009),
	TEEX_ERROR_INVALID_RESULT_FORMAT	= TEEX_MK_ERROR(0x200a),
	TEEX_ERROR_INVALID_WORKER_ADDR		= TEEX_MK_ERROR(0x200b),
	TEEX_ERROR_CONNECT_WORKER			= TEEX_MK_ERROR(0x200c),
	TEEX_ERROR_WORKER_EXECUTION			= TEEX_MK_ERROR(0x200d),
	
	TEEX_ERROR_INVALID_FILE				= TEEX_MK_ERROR(0x3001),
	TEEX_ERROR_UNHANDLED_FILE_TYPE		= TEEX_MK_ERROR(0x3002),

	TEEX_ERROR_INVALID_SERVICEINFO		= TEEX_MK_ERROR(0x4001),
	TEEX_ERROR_INVALID_SERVICEID_BUF	= TEEX_MK_ERROR(0x4002),
	TEEX_ERROR_INVALID_MANAGER_ADDR		= TEEX_MK_ERROR(0x4003),
	TEEX_ERROR_CONNECT_MANAGER			= TEEX_MK_ERROR(0x4004),
	TEEX_ERROR_MANAGER_INTERNAL			= TEEX_MK_ERROR(0x4005),


	TEEX_ERROR_UNIMPLEMENTED			= TEEX_MK_ERROR(0xffff),

} teex_status_t;


#endif
