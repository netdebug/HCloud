from teex import *
from hello_lib import *

input_string = TEEX_getinput()

output_string = hello_string(input_string)

TEEX_return({'message': output_string})

